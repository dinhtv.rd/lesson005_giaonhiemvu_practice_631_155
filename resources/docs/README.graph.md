# Document for making Graph

## File

* Store file to `resources/graphs/*.md`
* Config file path to `run_config.sh`

## Make

* Each line contains `pre_action`, `intent` (with `entities` or no), `next_action` which as an edge in the Graph. The
  form of a line is

```markdown
> previous_action > intent{"entity_1":"example_1, "entity_2":"example_2"} > next_action
```

Examples:

```markdown
> START > null_intent > action_start
> action_start > busy > action_busy
> action_ask_phone > provide_phone{"phone":""} > action_confirm_phone
```

* Start node is `START`
* Comment with form 
```
# your comment
```