import logging
from src.dev.preprocessors.message.modules import (
    LowerCase,
    RemoveSequentZeros,
    ReplaceWithDictionary,
    SuperNormMessage,
)

logger = logging.getLogger(__name__)


class PreprocessingMessage(object):
    def __init__(self):
        self.modules = [
            LowerCase(),
            RemoveSequentZeros(),
            ReplaceWithDictionary(),
            SuperNormMessage()
        ]

    def process(self, message):
        new_message = message
        for module in self.modules:
            try:
                new_message = module.process(message)
            except Exception as e:
                new_message = message
                logger.warning(f"process incoming message failed with exception {e}")
            message = new_message
        return new_message
