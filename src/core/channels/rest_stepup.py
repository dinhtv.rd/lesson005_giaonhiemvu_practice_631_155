import copy

from rasa.core.channels.channel import UserMessage, InputChannel
from rasa.core.channels.rest import QueueOutputChannel

import inspect
import logging
from asyncio import Queue
from sanic import Blueprint, response
from sanic.request import Request
from typing import Text, Dict, Any, Optional, Callable, Awaitable

from sanic.response import HTTPResponse

from src.core.initializer.drawio_extractor import DrawioExtractor
from src.core.utils.io import jsonio
from src.dev.context.define import CONFIG_RECORDS
from src.core.policies.graph_loader import GraphLoader
from config import PROJECT_NAME_CONFIG

logger = logging.getLogger(__name__)


class RestStepUp(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @staticmethod
    def fill_to_text(text, code, no, slots, current_record, language):
        if no == len(slots):
            return [{
                "text": text,
                "audio_id": code,
                "language": language
            }]
        answer = []
        slot = slots[no]
        condition_id = DrawioExtractor.extract_condition_code(slot)
        condition_values = current_record.get(condition_id).slots_for_init_text()
        for idx, value in enumerate(condition_values):
            text_new = text.replace("{" + f"{slot}" + "}", value)
            code_new = code + f"_{condition_id}_{idx}"
            answer.extend(RestStepUp.fill_to_text(text_new, code_new, no + 1, slots, current_record, language))
        return answer

    @staticmethod
    def fill_slot_to_text(text, current_record, code, language):

        code = f"{language}_{PROJECT_NAME_CONFIG.get('bot_name')}_" \
               f"{PROJECT_NAME_CONFIG.get('task_name')}_" \
               f"{PROJECT_NAME_CONFIG.get('project_version')}_" \
               f"{code}"

        if "{[" not in text:
            return [{
                "text": text,
                "audio_id": code,
                "language": language
            }]
        slots = DrawioExtractor.get_slots_from_text(text)
        answer = RestStepUp.fill_to_text(text, code, 0, slots, current_record, language)
        return answer

    def __init__(self):
        # get all template from node action
        self.conditions = copy.deepcopy(CONFIG_RECORDS)
        self.graph_controller = GraphLoader()
        nodes = copy.deepcopy(self.graph_controller.nodes)
        self.templates = []
        action_nodes = []
        for node_id in nodes:
            node = nodes.get(node_id)
            type_node = node.get("type_node")
            if type_node in ["bilingual_action", "english_action"]:
                action_nodes.append(copy.deepcopy(node))

        for node in action_nodes:
            tts_en = node.get("params").get("tts_en")[0]
            tts_vi = node.get("params").get("tts_vi")[0]
            code = node.get("id")
            if tts_en:
                self.templates.extend(self.fill_slot_to_text(tts_en, self.conditions, code, "en"))
            if tts_vi:
                self.templates.extend(self.fill_slot_to_text(tts_vi, self.conditions, code, "vi"))

    @classmethod
    def name(cls) -> Text:
        return "stepup"

    def blueprint(
            self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/templates", methods=["POST"])
        async def templates(request: Request) -> HTTPResponse:
            templates_answer = self.templates
            return response.json({
                "status": 0,
                "msg": "Success",
                "version": "v2",
                "templates": templates_answer
            })

        return custom_webhook