import aiohttp

import logging

logger = logging.getLogger(__name__)


async def json_async_request(url, payload, headers=None, timeout=20, method="POST"):
    try:
        async with aiohttp.ClientSession() as session:
            if method == "GET":
                async with session.get(url, data=payload, headers=headers, timeout=timeout) as response:
                    result = await response.json()
                    status = response.status
                    logger.debug(f"{method} {url} - {payload} - {status} - {result}")
                    return {
                        "result": result,
                        "status": status
                    }
            else:
                async with session.post(url, data=payload, headers=headers, timeout=timeout) as response:
                    result = await response.json()
                    status = response.status
                    logger.debug(f"{method} {url} - {payload} - {status} - {result}")
                    return {
                        "result": result,
                        "status": status
                    }
    except Exception as e:
        logger.debug(f"{url} request FAILED! With Exception: {e}")
        return {
            "result": None,
            "status": None
        }
