import logging
from xml.etree import cElementTree as ElementTree
import html


class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                if len(element) > 1 and element[1].tag == "Array" and element[0].tag == "mxCell":
                    print(element)
                    exit()
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)
            else:
                self.append(XmlDictConfig(element))


class XmlDictConfig(dict):
    '''
    Example usage:

    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)

    Or, if you want to use an XML string:

    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)

    And then use xmldict for what it is... a dict.
    '''

    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})


def xml2dict_from_file(path_file_xml):
    tree = ElementTree.parse(path_file_xml)
    root = tree.getroot()
    xmldict = XmlDictConfig(root)
    return xmldict


def dict2xml(d, root_node=None):
    if isinstance(d, dict) and "mxPoint" in d:
        list_mx_points = d.get("mxPoint")
        if isinstance(list_mx_points, list):
            other_points = []
            none_points = []
            tmp_list_mx_points = []
            for point in list_mx_points:
                if isinstance(point, list):
                    tmp_list_mx_points.extend(point)
                else:
                    tmp_list_mx_points.append(point)
            list_mx_points = tmp_list_mx_points.copy()
            for point in list_mx_points:
                if point.get("as"):
                    other_points.append(point)
                else:
                    none_points.append(point)
            if len(other_points) > 0 and len(none_points) > 0:
                d["mxPoint"] = other_points.copy()
                d["ARRAY"] = {
                    "as": "points",
                    "mxPoint": none_points
                }
    wrap = False if root_node is None or isinstance(d, list) else True
    root = 'objects' if root_node is None else root_node
    root_singular = root[:-1] if 's' == root[-1] and root_node is None else root
    xml = ''
    children = []
    if isinstance(d, dict):
        for key, value in dict.items(d):
            if isinstance(value, dict):
                children.append(dict2xml(value, key))
            elif isinstance(value, list):
                children.append(dict2xml(value, key))
            else:
                xml = xml + ' ' + key + '="' + str(html.escape(str(value), quote=True)) + '"'
    else:
        for value in d:
            children.append(dict2xml(value, root_singular))

    end_tag = '>' if 0 < len(children) else '/>'

    if wrap or isinstance(d, dict):
        xml = '<' + root + xml + end_tag

        if xml == '<mxPoint as="points">':
            xml = '<Array as="points">'
            root = "Array"
    if 0 < len(children):
        for child in children:
            xml = xml + child

        if wrap or isinstance(d, dict):
            xml = xml + '</' + root + '>'
    xml = xml.replace("ARRAY", "Array")
    return xml


def beautify_dict2xml(d, root_node=None):
    xml = dict2xml(d, root_node)
    root = ElementTree.fromstring(xml)
    from xml.dom import minidom
    xml_str = minidom.parseString(ElementTree.tostring(root)).toprettyxml(indent="  ")
    return xml_str
