from functools import cmp_to_key

from src.core.policies.fixed_condition_checker import remove_condition_id
from src.core.utils.io import jsonio

from config_platform import (
    PATH_EDGES_FULL_DUMPED,
    PATH_NODES_DUMPED,
    PATH_PRIORITY_RULE,
    VERTICES,
    FLOWS
)
import logging

logger = logging.getLogger(__name__)


class GraphLoader(object):
    def init_adjacency_list(self):
        for edge in self.edges:
            source = edge.get("source")
            target = edge.get("target")
            self.adjacency_list[source] = []
            self.adjacency_list[target] = []
            self.vertex_parent[source] = None
            self.vertex_parent[target] = None
            if self.is_vertex(source):
                self.vertex_parent[source] = source
            if self.is_vertex(target):
                self.vertex_parent[target] = target
        for edge in self.edges:
            source = edge.get("source")
            target = edge.get("target")
            self.adjacency_list[source].append(target)

        self.sort_adjacency_by_priority()

        for edge in self.edges:
            source = edge.get("source")
            target = edge.get("target")
            if self.is_vertex(target):
                # If source and target is a consecutive couple of actions and don't make a flow
                if self.is_vertex(source) and len(self.adjacency_list[source]) == 1:
                    self.vertex_parent[target] = source

    def __init__(self):
        with open(PATH_EDGES_FULL_DUMPED, "r") as filein:
            self.edges = jsonio.load_utf8(filein)
        with open(PATH_NODES_DUMPED) as filein:
            self.nodes = jsonio.load_utf8(filein)
        with open(PATH_PRIORITY_RULE) as filein:
            self.priority_rule = jsonio.load_utf8(filein)

        self.total_checkpoints = 0
        for node_id in self.nodes:
            node = self.nodes.get(node_id)
            if node.get("type_node") == "start_node":
                self.start_node = node_id
            if node.get("type_node") == "fixed_condition":
                desc = node.get("desc")
                desc = remove_condition_id(desc)
                if desc in ["checkpoint", "Checkpoint"]:
                    self.total_checkpoints += 1
        self.adjacency_list = {}
        self.vertex_parent = {}
        self.init_adjacency_list()

    def sort_adjacency_by_priority(self):

        def priority_compare(x, y):
            node_x = self.get_node(x)
            node_y = self.get_node(y)
            type_node_x = node_x.get("type_node")
            type_node_y = node_y.get("type_node")
            if type_node_x != type_node_y:
                node_rule = self.priority_rule.get("node")
                x_score = node_rule.index(type_node_x) if type_node_x in node_rule else -1
                y_score = node_rule.index(type_node_y) if type_node_y in node_rule else -1
                return x_score - y_score
            else:
                type_node = type_node_x
                if type_node == "intent_flow":
                    params_x = node_x.get("params")
                    params_y = node_y.get("params")
                    x_score = 0 if "intent_fallback" not in params_x else 1
                    y_score = 0 if "intent_fallback" not in params_y else 1
                    return x_score - y_score
                elif type_node == "condition_flow":
                    priority_x = node_x.get("priority")
                    priority_y = node_y.get("priority")
                    if not priority_x and not priority_y:
                        return 0
                    if priority_x and not priority_y:
                        return -1
                    if priority_y and not priority_x:
                        return 1
                    return priority_x - priority_y
                elif type_node == "fixed_condition":
                    params_x = node_x.get("desc")
                    params_y = node_y.get("desc")
                    x_score = 0 if "remain" not in params_x else 1
                    y_score = 0 if "remain" not in params_y else 1
                    return x_score - y_score
                else:
                    return 0

        for node_id in self.adjacency_list:
            adjacency = self.adjacency_list.get(node_id)
            adjacency = sorted(adjacency, key=cmp_to_key(priority_compare))
            self.adjacency_list[node_id] = adjacency

    def get_node(self, node_id):
        return self.nodes.get(node_id)

    def get_type_of_node(self, node_id):
        node = self.nodes.get(node_id)
        if not node:
            return None
        return node.get("type_node")

    def is_finish_action(self, node_id):
        node = self.nodes.get(node_id)
        if not node:
            return False
        return node.get("type_node") == "end_node"

    def get_adjacency_nodes(self, node_id):
        return self.adjacency_list.get(node_id)

    def is_vertex(self, node_id):
        if not node_id:
            return False
        return self.get_type_of_node(node_id) in VERTICES

    def is_flow(self, node_id):
        if not node_id:
            return False
        return self.get_type_of_node(node_id) in FLOWS
