import argparse
import os

from src.core.initializer.drawio_extractor import DrawioExtractor
from src.core.condition.condition_initializer import ConditionsInitializer
from src.core.api_metadata.api_metadata_initializer import APIMetadataInitializer
from config_platform import (
    PATH_INIT_LOGS
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", type=str, help="Input drawio scenario *.xml")

    args = parser.parse_args()
    assert os.path.exists(args.infile), "story-infile-edges {} not found!".format(args.infile)

    extractor = DrawioExtractor(args.infile)

    validate_result = extractor.validate()
    if len(validate_result) == 0:
        extractor.dump_files()
        with open(PATH_INIT_LOGS, "w") as fileout:
            fileout.write(f"Init from drawio successfully!")
        print("Init from drawio successfully!")
    else:
        print("Init failed, fix and re-build!")
        with open(PATH_INIT_LOGS, "w") as fileout:
            for log in validate_result:
                fileout.write(f"{log}\n")
        print(f"Error logs at {PATH_INIT_LOGS}")

    try:
        ci = ConditionsInitializer()
        ami = APIMetadataInitializer()
        print(f"Init condition classes successfully!")
    except Exception as e:
        print(f"Init condition classes Error: {e}")
